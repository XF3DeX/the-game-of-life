==The Game Of Life==
(Godot Engine Game)

Este es un proyecto de prueba para familiarzarme con el desarrollo dentro del motor Godot.
Es un juego simple donde se requiere hacer rebotar elementos sobre la cabeza del personaje.

Características del engine usadas:
	*2d scenes, scene transitions & instancing
	*sprites
	*animations
	*music & sound
	*scripting
	*menus & basic GUI
	*Mobile & desktop deployement
	*2D Particle System
	*Timers
	*basic input polling & Input Actions
	*basic 2d physics

Perdón por el código y la estructura general del juego. Al ser mi primer desarrollo en Godot, muchas cosas las fui aprendiendo sobre la marcha y quedaron "raras".
