
extends Node2D

var maximum = 1
var deadTime = 7

var generator
var elementos
var spawner
var deadTimer
var timerText
var gameOverNode
var player
var darkClouds

var level = 0 #times played
var maximums = [1,4,6,8,4] #how many things falling on each life
var deadtimes = [7,12,21,27,32] #how many time after the last element spawn on each life
var speeds = [5, 7, 8, 12, 5]
var subLifes = 5 #how many levels does each life takes

func _ready():
	get_tree().set_auto_accept_quit(false)

	generator = load("res://generator.scn").instance()
	elementos = get_node("elementos")
	spawner = get_node("spawner")
	timerText = get_node("TimerText")
	deadTimer = get_node("TimerText/timer")
	gameOverNode = get_node("GameOverNode")
	player = get_node("player")
	
	darkClouds = get_node("clouds 2")
	
	reset()

func _notification(p_notification):
	if (p_notification == MainLoop.NOTIFICATION_WM_QUIT_REQUEST && !gaveUp):
		giveUp()
	


var gaveUp = false
func _input(ev):

	if (Input.is_action_pressed("ui_cancel") && !gaveUp):
		giveUp()
	
func giveUp():

	gaveUp = true
	for c in elementos.get_children():
		elementos.remove_child(c)
	deadTimeOver()
	won = false
	showMessage()
	set_process_input(false)

func reset():

	set_process_input(true)

	for c in elementos.get_children():
		elementos.remove_child(c)
	spawner.start()
	deadTimer.time = deadTime
	#deadTimer.start()
	gameOverNode.get_node("anim").play("show")
	gameOverNode.get_node("anim").seek(0, true)
	gameOverNode.get_node("anim").stop()
	timerText.set_text("")
	
	if (getSubLife() == 0 && getLevel() != 0):
		nextStep()
	player.speed = speeds[player.CURRENT_AGE]
	
	gaveUp = false

func _on_disposeArea_body_enter( body ):
	elementos.remove_child(body)
	if (elementos.get_child_count() == 0):
		deadTimer.stop()
		deadTimeOver()
	
func _on_spawner_timeout():
	if (elementos.get_child_count() < maximum):
		spawnElement()
	if (elementos.get_child_count() >= maximum):
		deadTime()
	
	
func spawnElement():
	var e = generator.generateElement(player.CURRENT_AGE)
	randomize()
	e.set_pos(Vector2((randi()%600) +100 , -32))
	e.set_linear_velocity(Vector2(0, 0))
	elementos.add_child(e)
	
func deadTime():
	spawner.stop()
	if (player.CURRENT_AGE <= 4):
		deadTimer.start()
	
var won = false

func deadTimeOver():
	if (elementos.get_child_count() == maximum):
		won = true
	else:
		won = false
	stopElements()
	stopPlayer()
	showMessage()
	updateClouds()


func stopElements():
	for  i in elementos.get_children():
		i.set_sleeping(true)
		i.set_mode(RigidBody2D.MODE_KINEMATIC)
	spawner.stop()
	deadTimer.stop()

func stopPlayer():
	get_node("player").set_process_input(false)

func showMessage():
	gaveUp = true #fix!
	gameOverNode.get_node("saved").set_text(  str(elementos.get_child_count())+"/"+str(maximum)  )
	if(won):
		gameOverNode.get_node("label").set_text("You survived")
		gameOverNode.get_node("playBtn").set_text("Grow")
	else:
		gameOverNode.get_node("label").set_text("You ve lost it")
		gameOverNode.get_node("playBtn").set_text("Again?")
	gameOverNode.get_node("anim").play("show")

func updateClouds():
	var wb = get_node("white_bg")
	var bg = get_node("bg")
	if (won):
		for c in darkClouds.get_children():
			if (c.get_type() == "Sprite"):
				c.set_modulate(c.get_modulate().linear_interpolate(Color(1, 1, 1, 1), 0.1))
		wb.set_modulate(wb.get_modulate().linear_interpolate(Color(1, 1, 1, 1), 0.05))
		bg.set_modulate(bg.get_modulate().linear_interpolate(Color(1, 1, 1, 1), 0.2))
	else:
		for c in darkClouds.get_children():
			if (c.get_type() == "Sprite"):
				c.set_modulate(c.get_modulate().linear_interpolate(Color(0, 0, 0, 1), 0.1))
		wb.set_modulate(wb.get_modulate().linear_interpolate(Color(0, 0, 0, 1), 0.05))
		bg.set_modulate(bg.get_modulate().linear_interpolate(Color(0, 0, 0, 1), 0.2))

func nextStep():
	player.grow()
	maximum = maximums[player.CURRENT_AGE]
	deadTime = deadtimes[player.CURRENT_AGE]
	
	print("NEXT STEP: dt=", deadTime, " m=", maximum)

func _on_playBtn_pressed():
	# RELOAD LEVEL
	deadTimer.stop()
	if (won):
		level+=1
		maximum += 1
	reset()
	
func _on_backBtn_pressed():
	get_node("/root/global").goto_scene("res://menu.scn")
	
func getLevel():
	return level
	
func getSubLife():
	return level % subLifes



func _on_btnLeft_pressed():
	print("LEEEEEEEEEEEEEEFTTTT!")
	pass # replace with function body
