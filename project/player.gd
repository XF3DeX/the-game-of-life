
extends Node2D

export(float) var rebote = 250
var speed = 1
var size

var animation;

const BABY = 0
const CHILD = 1
const YOUNG = 2
const ADULT = 3
const OLD = 4
var CURRENT_AGE = BABY


func _ready():
	set_process(true)
	size = get_viewport_rect().size
	animation = get_node("animation")
	setupAnimation()

func setupAnimation():
	animation.play("base")
	animation.seek(0,true)
	playCurrentAnimation()
	animation.stop()
	animation.seek(0, true)

func _on_Area2D_body_enter( body ):
	body.set_linear_velocity(Vector2(0, -rebote));

func _process(delta):
	if (Input.is_action_pressed("ui_left")):
		set_pos( get_pos()-Vector2(speed*delta*60,0) )
		getCurrentSprite().set_flip_h(false)
	if (Input.is_action_pressed("ui_right")):
		set_pos( get_pos()+Vector2(speed*delta*60,0) )
		getCurrentSprite().set_flip_h(true)
	
#	var g = Input.get_accelerometer()
#	var movement = min(max(-3, g.x), 3)
#	
#	if (movement < 0):
#		set_pos(get_pos() - Vector2(speed*(movement/3), 0))
#		getCurrentSprite().set_flip_h(true)
#	elif(movement > 0):
#		set_pos(get_pos() - Vector2(speed*(movement/3), 0))
#		getCurrentSprite().set_flip_h(false)
	
	var p = get_pos()
	p.x = min(max(0, p.x), size.x)
	set_pos(p)

func grow():
	playCurrentAnimation()
	CURRENT_AGE = min(CURRENT_AGE + 1, 4)
		
func getCurrentSprite():
	if (CURRENT_AGE == BABY):
		return get_node("baby")
	elif (CURRENT_AGE == CHILD):
		return get_node("child")
	elif (CURRENT_AGE == YOUNG):
		return get_node("young")
	elif (CURRENT_AGE == ADULT):
		return get_node("adult")
	elif (CURRENT_AGE == OLD):
		return get_node("old")
	else:
		return get_node("old")

func playCurrentAnimation():
	if (CURRENT_AGE == BABY):
		animation.play("babyToChild")
	elif (CURRENT_AGE == CHILD):
		animation.play("childToYoung")
	elif (CURRENT_AGE == YOUNG):
		animation.play("youngToAdult")
	elif (CURRENT_AGE == ADULT):
		animation.play("adultToOld")
	elif (CURRENT_AGE == OLD):
		animation.play("old")