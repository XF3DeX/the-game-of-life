
extends Node2D

func _ready():
	pass

func generateElement(tipo):
	randomize()
	var n = 0
	if (tipo == 0):#PLAYER.BABY
		n = randi() % 3
		if (n == 0):
			return get_node("almohada").duplicate()
		if (n == 1):
			return get_node("chupete").duplicate()
		if (n == 2):
			return get_node("bear").duplicate()
	elif (tipo == 1):#PLAYER.CHILD
		n = randi() % 5
		if (n == 0):
			return get_node("bicicle").duplicate()
		if (n == 1):
			return get_node("book").duplicate()
		if (n == 2):
			return get_node("sword").duplicate()
		if (n == 3):
			return get_node("ball").duplicate()
		if (n == 4):
			return get_node("joystick").duplicate()
	elif (tipo == 2):#PLAYER.YOUNG
		n = randi() % 8
		if (n == 0):
			return get_node("music").duplicate()
		if (n == 1):
			return get_node("headphones").duplicate()
		if (n == 2):
			return get_node("money").duplicate()
		if (n == 3):
			return get_node("pizza").duplicate()
		if (n == 4):
			return get_node("cellphone").duplicate()
		if (n == 5):
			return get_node("lips").duplicate()
		if (n == 6):
			return get_node("joystick").duplicate()
		if (n == 7):
			return get_node("joystick").duplicate()
	elif (tipo == 3):#PLAYER.ADULT
		n = randi() % 7
		if (n == 0):
			return get_node("money").duplicate()
		if (n == 1):
			return get_node("money").duplicate()
		if (n == 2):
			return get_node("pizza").duplicate()
		if (n == 3):
			return get_node("hammer").duplicate()
		if (n == 4):
			return get_node("lips").duplicate()
		if (n == 5):
			return get_node("baby").duplicate()
		if (n == 6):
			return get_node("briefcase").duplicate()
	elif (tipo == 4):#PLAYER.OLD
		n = randi() % 3
		if (n == 0):
			return get_node("dental").duplicate()
		if (n == 1):
			return get_node("baby").duplicate()
		if (n == 2):
			return get_node("walker").duplicate()

